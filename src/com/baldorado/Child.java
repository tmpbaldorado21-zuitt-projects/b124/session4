package com.baldorado;

public class Child extends human{
    private String education;

    public Child(){}

    public Child (String name, int age, char gender, String newEducation) {
        super(name, age, gender);
        this.education = newEducation;
    }

    public String getEducation() {
        return this.education;
    }
    public void setEducation(String newEducation) {
        this.education = newEducation;
    }

    public String talk() {
        return "My education is " + this.getEducation();
    }
}
