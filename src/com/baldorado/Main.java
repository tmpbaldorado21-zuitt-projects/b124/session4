package com.baldorado;

public class Main {

    public static void main(String[] args) {

        //Mini-activity:
        // Create a new human class with the following properties and methods:
        //Properties:
        //name, age, gender (single character)
        //Methods:
        //talk --> display a message to the console "Hello, my name is "concatenated to the user's name"
        //setters and getters method for the properties
        //Constructor: implement both empty and parameterized

        System.out.println("** Human Class **");

        human humanObject = new human("Tricia ", 26, 'F');
        System.out.println(humanObject.talk ());

        Adult adultObject = new Adult("Tricia ", 26, 'F', "Engineer" );
        System.out.println(adultObject.talk ());

        Child childObject = new Child("Tricia ", 26, 'F', "Financial Management" );
        System.out.println(childObject.talk());

    }
}
