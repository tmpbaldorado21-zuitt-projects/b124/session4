package com.baldorado;

public class Adult  extends human{
    private String occupation;

    public Adult(){}

    public Adult(String name, int age, char gender, String newOccupation){
        super(name, age, gender);
        this.occupation = newOccupation;
    }

    public String getOccupation() {
        return this.occupation;
    }
    public void setOccupation(String newOccupation) {
        this.occupation = newOccupation;
    }

    public String talk() {
        return "My occupation is " + this.getOccupation();
    }
}
