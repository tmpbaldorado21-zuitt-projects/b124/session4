package com.baldorado;

public class human {
    private String name;
    private int age;
    private char gender;

    public human() {
        System.out.println("A new human object was created.");
    }

    public human(String newName, int newAge, char newGender) {
        System.out.println("A new human object was created.");
        this.name = newName;
        this.age = newAge;
        this.gender = newGender;
    }

    public String getName() {
        return this.name;
    }
    public void setName(String newName) {
        this.name = newName;
    }

    public char getGender() {
        return this.gender;
    }
    public int getAge() {
        return this.age;
    }

    public void setGender(char newGender){
        this.gender = newGender;
    }

    public void setAge(int newAge){
        this.age = newAge;
    }

    public String talk() {
        return "Hello my name is " + this.getName();
        }
    }
